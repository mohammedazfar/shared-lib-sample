def call(String env_name = 'default') {
   if (env_name == "dev") {
       return "10.0.0.1"
   }
   else if (env_name == "devint") {
       return "10.1.0.1"
   }
   else if (env_name == "qa") {
       return "10.2.0.1"
   }
   else if (env_name == "prod") {
       return "10.3.0.1"
   }
   else {
       return "error"
   }
}

def call(String account = '664231006841' ) {
   sh """
   aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ${account}.dkr.ecr.us-east-1.amazonaws.com
   docker tag nginx ${account}.dkr.ecr.us-east-1.amazonaws.com/nginx:latest
   docker push ${account}.dkr.ecr.us-east-1.amazonaws.com/nginx:latest
   """
}

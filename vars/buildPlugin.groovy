def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    // now build, based on the configuration provided
    node('ci-slave') {
        git url: "https://github.com/jenkinsci/${config.name}-plugin.git"
        //sh "mvn install"
        //mail to: "...", subject: "${config.name} plugin build", body: "..."
        echo "testing"
        stage("phase1")
        {
            echo "phase1"
        }
        stage("phase2")
        {
            echo "phase2"
        }
    }
    node('cd-slave') {
        git url: "https://github.com/jenkinsci/${config.name}-plugin.git"
        //sh "mvn install"
        //mail to: "...", subject: "${config.name} plugin build", body: "..."
        echo "testing"
        stage("phase1")
        {
            echo "phase1"
        }
        stage("phase2")
        {
            echo "phase2"
        }
    }
}
